﻿using UnityEngine;
using System;
using System.Collections;

public class SimpleSpiral : MonoBehaviour {


float circleSpeed = 1f;
float forwardSpeed = -10f; // Assuming negative Z is towards the camera
float circleSize = 0.25f;
float circleGrowSpeed = 0.025f;
Vector3 tempPos;
	// Use this for initialization
	void Start () {
	tempPos = transform.position;
	}
	
	
 
void Update()
{
  
       
        
       tempPos.x =  Mathf.Sin(Time.time * circleSpeed) * circleSize; 
    tempPos.y = Mathf.Cos(Time.time * circleSpeed) * circleSize;
    
     tempPos.z = forwardSpeed * Time.deltaTime;
    
    circleSize += circleGrowSpeed;
        transform.position = tempPos;
   
}
}
