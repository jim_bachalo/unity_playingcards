﻿Shader "Custom/TwoSided" {
	Properties {
		_FirstTex ("First (RGB)", 2D) = "white" {}
		_SecondTex ("Second (RGB)", 2D) = "white"{}
	}


	
	SubShader {



		
    Pass {
       
      Cull Off // turn off backface culling
      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
       #pragma target 3.0
      #include "UnityCG.cginc"


		sampler2D _FirstTex;
		sampler2D _SecondTex;
	 
  
      struct v2f {
          float4 pos : SV_POSITION;
          fixed4 color : COLOR;
          float4 tex : TEXCOORD0;
          //fixed facing : VFACE;
      };



      v2f vert (appdata_base v)
      {
          v2f o;          
          o.pos = mul (UNITY_MATRIX_MVP, v.vertex);          
          float3 viewVector = normalize(_WorldSpaceCameraPos);
          
          //Converting the normal to world space as we have view vector in world space. We need to transfer both to same space for any operations.
          float3 worldSpaceNormal = normalize(mul((float3x3)_World2Object,v.normal));
          
          //Dot product to know which side we are facing
        float NdotL = dot(viewVector,v.normal);
         

          o.color.w = NdotL;
          o.tex = v.texcoord;        
          return o;
      }


      fixed4 frag (v2f i, fixed facing : VFACE) : COLOR0 { 
	     //if(i.color.w > 0.0f)
	       if(facing > 0.0f)
	      {
	      	i.color.xyz = tex2D(_FirstTex, float2(i.tex.xy)).xyz;
	      }
	      else
	      {
	      	i.color.xyz = tex2D(_SecondTex, float2(i.tex.xy)).xyz;
	      }
	     
	      return i.color; 
      }
      ENDCG
    }// end pass






  } // end subShader
} //end Shader